package org.vaadin.uitracker.demo;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.vaadin.uitracker.UITracker;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.ListSeries;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;
import com.vaadin.annotations.Title;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("UITracker Demo")
@SuppressWarnings("serial")
public class DemoUI extends UI {

    @Override
    protected void init(VaadinRequest request) {

        // Show it in the middle of the screen
        final VerticalLayout layout = new VerticalLayout();
        setContent(layout);
        layout.setSpacing(true);
        layout.setMargin(true);
        layout.addComponent(new Label("object which is not tracked"));
        layout.addComponent(addButton("first button"));
        layout.addComponent(addButton("second button"));
        layout.addComponent(addButton("third button"));
        layout.addComponent(addDateField());

        final HorizontalLayout layoutForListenerOutput = new HorizontalLayout();
        UiTrackerListener listener = new UiTrackerListener(layoutForListenerOutput);
        
        UITracker tracker = new UITracker(listener);
        tracker.extend(layout);
        
        layout.addComponent(layoutForListenerOutput);
        layout.setComponentAlignment(layoutForListenerOutput, Alignment.BOTTOM_RIGHT);
 
    }
    
    private Component addDateField() {
    	PopupDateField dateField = new PopupDateField("date field");
        dateField.setValue(new Date());
        dateField.setImmediate(true);
        dateField.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateField.setLocale(Locale.US);
        dateField.setResolution(Resolution.MINUTE);
    	dateField.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(final ValueChangeEvent event) {
                final String valueString = String.valueOf(event.getProperty()
                        .getValue());
                Notification.show("Value changed:", valueString,
                        Type.TRAY_NOTIFICATION);
            }
        });
    	return dateField;
	}

	Button addButton(String id)
    {
    	Button button = new Button(id);
    	button.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                Notification.show("The button was clicked", Type.HUMANIZED_MESSAGE);
            }
        });
    	return button;
    }

}
