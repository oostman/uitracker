package org.vaadin.uitracker.demo;

import java.util.ArrayList;
import java.util.List;

import org.vaadin.uitracker.UiClickListener;
import org.vaadin.uitracker.client.ClickedObject;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.ListSeries;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;
import com.vaadin.ui.HorizontalLayout;

public class UiTrackerListener implements UiClickListener {

	public UiTrackerListener(HorizontalLayout layout)
	{
		this.layout = layout;
		layout.addComponent(initChart());
	}
	
	HorizontalLayout layout;
	TrackedObjects clicks = new TrackedObjects();
	
	@Override
	public void clickReceived(ClickedObject click) {
		clicks.addClick(click);
		
		refreshChart();
	}

	private void refreshChart() {
		series.setData(clicks.getFrequencies());
		xaxis.setCategories(clicks.getTrackedElements());
		chart.drawChart();
	}
	
	ListSeries series;
	XAxis xaxis;
	Chart chart;

	private void addTrackedObjectsToChart(Configuration conf) {
		// The data
        series = new ListSeries("Number of clicks");
        series.setData(clicks.getFrequencies());
        conf.addSeries(series);

        // Set the category labels on the axis correspondingly
        xaxis = new XAxis();
        xaxis.setCategories(clicks.getTrackedElements());
        xaxis.setTitle("UI elements");
        conf.addxAxis(xaxis);
	}

	private Chart initChart() {
		chart = new Chart(ChartType.BAR);
        chart.setWidth("400px");
        chart.setHeight("300px");
        
        Configuration conf = chart.getConfiguration();
        conf.setTitle("Awesome charts of the usage of the page");
        conf.setSubTitle("Bringing big brother to your local network");
        
        addTrackedObjectsToChart(conf);
        
    	 // Set the Y axis title
        YAxis yaxis = new YAxis();
        yaxis.getLabels().setStep(2);
        yaxis.setTitle("");
        conf.addyAxis(yaxis);
		return chart;
	}

}
