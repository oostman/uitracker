package org.vaadin.uitracker.demo;

import java.util.ArrayList;
import java.util.List;

import org.vaadin.uitracker.client.ClickedObject;

public class TrackedObjects {
	
	public List<ObjectUsage> trackedObjects = new ArrayList<ObjectUsage>();
	
	void addClick(ClickedObject click)
	{
		for(ObjectUsage current : trackedObjects)
		{
			if(current.id.equals(click.innerText))
			{
				current.addUsage();
				return;
			}
		}
		ObjectUsage newObject = new ObjectUsage(click.innerText, click.elementClass);
		trackedObjects.add(newObject);
		
	}
	
	public List<Number> getFrequencies() {
		List<Number> frequencies = new ArrayList<Number>();
		
		for(ObjectUsage object : trackedObjects)
		{
			if(isOfClass(object, "v-button") || isOfClass(object, "v-datefield"))
			{
				frequencies.add(object.frequency);
			}
		}
		
		return frequencies;
	}

	public String[] getTrackedElements() {
		String[] ids = new String[trackedObjects.size()];
		
		for(int i = 0; i<trackedObjects.size(); i++)
		{
			if(isOfClass(trackedObjects.get(i), "v-button"))
			{
				ids[i] = "Button: " + trackedObjects.get(i).id;
			}
			if(isOfClass(trackedObjects.get(i), "v-datefield"))
			{
				ids[i] = "DateField: " + trackedObjects.get(i).id;
			}
			
		}
		
		return ids;
	}

	private boolean isOfClass(ObjectUsage object, String classStart) {
		return object.elementClass.contains(classStart);
	}

	public Integer getNumberOfObjects() {
		return trackedObjects.size();
	}
	
	class ObjectUsage {
		String id;
		String elementClass;
		Integer frequency;
		public ObjectUsage(String objectId, String elemClass)
		{
			id = objectId;
			elementClass = elemClass;
			frequency = 1;
		}
		
		public void addUsage()
		{
			frequency = frequency + 1;
		}
		
		public Integer getUsage()
		{
			return frequency;
		}
	}
}
