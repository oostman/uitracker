package org.vaadin.uitracker.client;

public class ClickedObject {
	public String innerText;
	public String elementClass;
	public long timeStamp;
	
	public ClickedObject() {
		// only for serializing
	}
	
	public ClickedObject(String innerText, String elementClass, long timeStamp)
	{
		this.innerText = innerText;
		this.elementClass = elementClass;
		this.timeStamp = timeStamp;
	}
}
