package org.vaadin.uitracker.client;

import com.vaadin.shared.communication.ServerRpc;

public interface UITrackerServerRpc extends ServerRpc {

    public void clicked(ClickedObject click);

}
