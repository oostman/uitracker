package org.vaadin.uitracker;

import org.vaadin.uitracker.client.ClickedObject;


public interface UiClickListener {
	void clickReceived(ClickedObject click);
}
