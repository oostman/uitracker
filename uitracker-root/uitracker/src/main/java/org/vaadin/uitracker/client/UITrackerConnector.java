package org.vaadin.uitracker.client;


import org.vaadin.uitracker.UITracker;

import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.vaadin.client.ComponentConnector;
import com.vaadin.client.ServerConnector;
import com.vaadin.client.VConsole;
import com.vaadin.client.communication.RpcProxy;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.extensions.AbstractExtensionConnector;
import com.vaadin.shared.ui.Connect;

// Connector binds client-side widget class to server-side component class
// Connector lives in the client and the @Connect annotation specifies the
@Connect(UITracker.class)
public class UITrackerConnector extends AbstractExtensionConnector {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5413607254085420621L;
	// ServerRpc is used to send events to server. Communication implementation
    // is automatically created here
    UITrackerServerRpc rpc = RpcProxy.create(UITrackerServerRpc.class, this);

    public UITrackerConnector() {}

    // Whenever the state changes in the server-side, this method is called
    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);

    }

    @Override
    protected void extend(ServerConnector target) {    
    	final Panel widget = (Panel) ((ComponentConnector) target).getWidget();
    	TrackerWidget trackerWidget = new TrackerWidget();
    	widget.add(trackerWidget);
    	
		DOM.setCapture(trackerWidget.getElement());
    }

    private class TrackerWidget extends HTML{
    	@Override
    	public void onBrowserEvent(Event event) {
    		super.onBrowserEvent(event);
    		
    		if(event.getType().equals(BrowserEvents.CLICK))
    		{
    			Element target = Element.as(event.getEventTarget());
    			VConsole.log(target.getInnerText());
    			
    			ClickedObject click = new ClickedObject(target.getInnerText(), target.getClassName(), System.currentTimeMillis());
    			rpc.clicked(click);
    		}
    	}
    }
    
}
