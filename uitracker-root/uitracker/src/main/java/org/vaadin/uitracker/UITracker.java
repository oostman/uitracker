package org.vaadin.uitracker;

import org.vaadin.uitracker.client.ClickedObject;
import org.vaadin.uitracker.client.UITrackerServerRpc;

import com.vaadin.server.AbstractExtension;
import com.vaadin.ui.AbstractLayout;

// This is the server-side UI component that provides public API
// for UITracker
public class UITracker extends AbstractExtension {

	final UiClickListener outsideListener;
	
    // To process events from the client, we implement ServerRpc
    private UITrackerServerRpc rpc = new UITrackerServerRpc() {

        // Event received from client - user clicked our widget
		@Override
		public void clicked(ClickedObject click) {
			outsideListener.clickReceived(click);
		}
    };

    public void extend(AbstractLayout target) {
        super.extend(target);
    }

    public UITracker(UiClickListener listener) {
    	outsideListener = listener;

        // To receive events from the client, we register ServerRpc
        registerRpc(rpc);
    }
}
